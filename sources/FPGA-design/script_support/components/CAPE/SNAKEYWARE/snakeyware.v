module snakey(
input              clk,
input              resetn,
output  reg [10:0] leds
);

wire		tick	  = (counter == 5000000);
reg [24:0]  counter   = 0;
reg			direction = 0;

always@(posedge clk or negedge resetn) begin
    if(~resetn) begin
		leds	  <= 11'b00011111000;
		direction <= ~direction;
        counter   <= 0;
    end else begin
		if (tick) begin
			if (direction)
				leds <= { leds[9:0], leds[10] };
			else
				leds <= { leds[0], leds[10:1] };	

			counter <= 0;
		end else begin
        	counter <= counter + 1;
		end
    end
end
endmodule
